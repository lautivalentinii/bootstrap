$(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover()
    $('.carousel').carousel({
      interval: 2000
    });
  });

  $('#reserva').on('show.bs.modal', function (e) {
    $('#reservaBtn').removeClass('btn-primary');
    $('#reservaBtn').addClass('btn-secondary');
    $('#reservaBtn').prop('disabled', true);
})
  $('#reserva').on('hide.bs.modal', function (e) {
    $('#reservaBtn').removeClass('btn-secondary');
    $('#reservaBtn').addClass('btn-primary');
    $('#reservaBtn').prop('disabled', false);
})